class CartItemsController < ApplicationController
    def create
        @cart_item = Cartitem.new(qty: params[:qty],
            product_id: params[:product_id],
            cart_id: current_cart.id
            )
        if @cart_item.save
          redirect_to top_main_path
        else
          render top_main_path
        end
    end
    
    def destroy
        cart_item = Cartitem.where(params[:product_id])
        cart_item.destroy_all
        redirect_to top_main_path
    end
end
